## test de modif


from django.db import models
from django.contrib.gis.db import models
import datetime
from django.utils import  timezone
from django.urls import reverse


class Departement(models.Model):
    insee_dep = models.CharField(primary_key=True, max_length=3, blank=True, null=False)
    nom_dep_m = models.CharField(max_length=50, blank=True, null=True)
    nom_dep = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        ordering = ["nom_dep_m"]

    def __str__(self):
        return self.nom_dep_m


class Commune(models.Model):
    insee_com = models.CharField(primary_key=True, max_length=5, blank=True, null=False)
    insee_dep = models.ForeignKey(Departement, on_delete=models.CASCADE)
    nom_com_m = models.CharField(max_length=50, blank=True, null=True)
    nom_com = models.CharField(max_length=45, blank=True, null=True)
    # commentaire = models.TextField(max_length=200, blank=True, null=True)

    class Meta:
        ordering = ["nom_com_m"]

    def __str__(self):
        return self.nom_com_m


class Site(models.Model):
    nom_site = models.CharField(max_length=50, blank=True, null=True)
    code_site = models.CharField(max_length=20, blank=True, null=True)
    departement = models.ForeignKey(Departement, on_delete=models.CASCADE, blank=True, null=True)
    commune = models.ForeignKey(Commune, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.nom_site


class Communes(models.Model):
    insee_com = models.CharField(max_length=5, blank=True, null=True)
    insee_dep = models.CharField(max_length=3, blank=True, null=True)
    nom_com_m = models.CharField(max_length=50, blank=True, null=True)
    nom_com = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.nom_com_m


class Ruchers(models.Model):
    nom_rucher = models.CharField(max_length=25, blank=True, null=True)
    code_rucher = models.CharField(max_length=20, blank=True, null=True)
    dept = models.CharField (max_length=2, blank=True, null=True)
    commune = models.ForeignKey(Communes, on_delete=models.CASCADE, blank=True, null=True)
    lieudit = models.CharField(max_length=50, blank=True, null=True)
    statut_rucher = models.BooleanField(blank=True, null=False, default=False)
    bio = models.BooleanField(blank=True, null=False, default=False)
    geom = models.PointField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('gestruc:rucher-detail', kwargs={'pk': str(self.pk)})

    def __str__(self):
        return self.nom_rucher


class Typ_ruche(models.Model):
    ruchette = models.BooleanField(blank=True, null=False, default=False)
    modele = models.CharField(max_length=30, blank=True, null=True)
    description = models.TextField (max_length=200, blank=True, null=True)

    def __str__(self):
        return self.modele


class Ruches(models.Model):
    code_ruche = models.CharField(max_length=10, blank=False, null=False, unique=True)
    type_ruche = models.ForeignKey(Typ_ruche, on_delete=models.CASCADE, blank=True, null=True)
    date_mes = models.DateField(("Date mise en service"), default=datetime.date.today)
    statut_bio = models.BooleanField(blank=True, null=False, default=False)
    affectation = models.BooleanField(default = False)
    commentaire = models.TextField(max_length=200, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('gestruc:ruche-detail', kwargs={'pk': str(self.pk)})

    def __str__(self):
        return self.code_ruche


class Note_etat(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=20, blank=True, null=True)

    def __str__(self):
        return self.description


class Phenotypes(models.Model):
    phenotype = models.CharField(max_length=10, blank=True, null=True)
    description = models.TextField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.phenotype


class Note_agress(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Note_essaim(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Note_prod(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Note_varroa(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Colonies(models.Model):
    code_col = models.CharField(max_length=20, blank=True, null=False, unique=True)
    annee_reine = models.CharField(max_length=4, blank=False, null=False, default='2000')
    phenotype = models.ForeignKey(Phenotypes, on_delete=models.CASCADE)
    agressivite = models.ForeignKey(Note_agress, on_delete=models.CASCADE)
    essaimage = models.ForeignKey(Note_essaim, on_delete=models.CASCADE)
    production = models.ForeignKey(Note_prod, on_delete=models.CASCADE)
    varroa = models.ForeignKey(Note_varroa, on_delete=models.CASCADE)
    existante = models.BooleanField(default = True)
    affectation = models.BooleanField(default = False)
    commentaire = models.TextField(max_length=200, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('gestruc:colonie-detail', kwargs={'pk': str(self.pk)})

    def __str__(self):
        return self.code_col


class Ruruchcol(models.Model):
    rucher = models.ForeignKey(Ruchers, related_name='rucher', on_delete=models.CASCADE,null=False)
    ruche = models.ForeignKey(Ruches,related_name='ruche', on_delete=models.CASCADE, null=False)
    colonie = models.ForeignKey(Colonies,related_name='colonie', on_delete=models.CASCADE, null=False)
    emplacement = models.CharField(max_length=10, blank=True, null=True)
    debut = models.DateTimeField('Date début')
    fin = models.DateTimeField('Date de fin', blank =True, null=True)
    commentaire = models.TextField(max_length=240, blank=True, null=True)

    def ruruchcol_encours(self):
        return self.objects.filter(fin__isnull=True)
     
    def get_absolute_url(self):
        return reverse('gestruc:Affruruchcol_detail', kwargs={'pk': str(self.pk)})

    def __str__(self):
       return 'ruche={0}, colonie={1}'.format(self.ruche.code_ruche, self.colonie.code_col)
       #return 'rucher={0}, ruche={1}, colonie={2}'.format(self.rucher.nom_rucher, self.ruche.code_ruche, self.colonie.code_col)


class Operateur(models.Model):
    pseudo = models.CharField(max_length=20)
    nom = models.CharField(max_length=20, blank=True, null=True)
    prenom = models.CharField(max_length=20, blank=True, null=True)
    infos = models.TextField(max_length=200, blank=True, null=True)
    def __str__(self):
        return self.pseudo


class Typ_visite(models.Model):
    lib_visit = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField (max_length=200, blank=True, null=True)

    def __str__(self):
        return self.lib_visit


class Motif_visite(models.Model):
    lib_motif = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField (max_length=200, blank=True, null=True)

    def __str__(self):
        return self.lib_motif


class Etat_ponte(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Etat_couv_ouvert(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Etat_couv_ferme(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Etat_miel(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Etat_pollen(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Activite_reine(models.Model):
    libelle = models.CharField(max_length=20, null=True )
    description = models.TextField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.libelle


class Note_globale(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Pression_frelon(models.Model):
    note = models.CharField(max_length=2)
    description = models.TextField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.description


class Gestion_hausses(models.Model):
    libelle = models.CharField(max_length=20, null=True )
    description = models.TextField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.libelle


class Visites(models.Model):
    datevis     =models.DateTimeField('date Visite')
    operateur = models.ManyToManyField(Operateur)
    rucher = models.ForeignKey(Ruchers, on_delete=models.CASCADE,null=True)
    ide_visit = models.CharField(max_length=20, blank=False, null=True, unique=True)
    affectation = models.ForeignKey(Ruruchcol, on_delete=models.CASCADE,null=True)
    type_visite = models.ForeignKey(Typ_visite, on_delete=models.CASCADE, null=True)
    motif_visite = models.ForeignKey(Motif_visite, on_delete=models.CASCADE, null=True)
    nb_cadres = models.IntegerField(null=False, default=0)
    ponte = models.ForeignKey(Etat_ponte, on_delete=models.CASCADE, null=True)
    couv_ouvert = models.ForeignKey(Etat_couv_ouvert, on_delete=models.CASCADE, null=True)
    couv_ferme = models.ForeignKey(Etat_couv_ferme, on_delete=models.CASCADE, null=True)
    miel = models.ForeignKey(Etat_miel, on_delete=models.CASCADE, null=True)
    pollen = models.ForeignKey(Etat_pollen, on_delete=models.CASCADE, null=True)
    varroa = models.ForeignKey(Note_varroa,  on_delete=models.CASCADE, null=True)
    agressiv = models.ForeignKey(Note_agress, on_delete=models.CASCADE, null=True)
    act_reine = models.ForeignKey(Activite_reine, on_delete=models.CASCADE, null=True)
    reine_vue = models.BooleanField(default=False)
    frelon = models.ForeignKey(Pression_frelon, on_delete=models.CASCADE, null=True)
    hausses = models.ForeignKey(Gestion_hausses, on_delete=models.CASCADE, null=True)
    commentaire = models.TextField(max_length=200, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('gestruc:visite_detail', kwargs={'pk': str(self.pk)})

    def __str__(self):
        return self.ide_visit


    # def visites_recentes(self):
    #     return self.datevis >= timezone.now() - datetime.timedelta(days=30)

class Unites(models.Model):
    unite = models.CharField(max_length=20, blank=True, null=True)
    commentaire = models.TextField (max_length=50, blank=True, null=True)

    def __str__(self):
        return self.unite


class Interv(models.Model):
    date =models.DateTimeField('date intervention')
    operateur = models.ManyToManyField(Operateur)
    rucher = models.ForeignKey(Ruchers, on_delete=models.CASCADE,null=True)
    affectation = models.ForeignKey(Ruruchcol, on_delete=models.CASCADE,null=True)
    libelle = models.CharField(max_length=70, blank=True, null=False)


class Typ_nourrissement(models.Model):
    typ_nour = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField (max_length=200, blank=True, null=True)

    def __str__(self):
        return self.typ_nour

class Nat_nourrissement(models.Model):
    nature_nour = models.CharField(max_length=50, blank=True, null=True)
    description = models.TextField (max_length=200, blank=True, null=True)

    def __str__(self):
        return self.nature_nour

class Nourrissement(models.Model):
    datenourr = models.DateTimeField('date intervention')
    operateur = models.ManyToManyField(Operateur)
    rucher = models.ForeignKey(Ruchers, on_delete=models.CASCADE,null=True)
    affectation = models.ManyToManyField(Ruruchcol)
   #  affectation = models.ForeignKey(Ruruchcol, on_delete=models.CASCADE,null=True)
    typnour = models.ForeignKey(Typ_nourrissement, on_delete=models.CASCADE,null=True)
    natnour = models.ForeignKey(Nat_nourrissement, on_delete=models.CASCADE,null=True)
    qte = models.DecimalField(max_digits=6, decimal_places=2)
    unit = models.ForeignKey(Unites, on_delete=models.CASCADE,null=True)
    commentaire= models.CharField(max_length=70, blank=True, null=False)

    def get_absolute_url(self):
        return reverse('gestruc:nour_update', kwargs={'pk': str(self.pk)})


class Nat_cadres(models.Model):
    libelle = models.CharField(max_length=20, blank=True, null=False)
    description =  models.CharField(max_length=70, blank=True, null=False)

    def __str__(self):
        return self.libelle

class Mvts_cadres(models.Model):
    date_ech = models.DateTimeField('date intervention')
    idmvt = models.CharField(max_length=20, blank=True, null=False)
    contexte = models.CharField(max_length=70, blank=True, null=False)
    sitesrc =  models.ForeignKey(Ruchers, on_delete=models.CASCADE,null=True, related_name='+')
    ruchsrc =  models.ForeignKey(Ruches, on_delete=models.CASCADE,null=True, related_name='+')
    sitedst =  models.ForeignKey(Ruchers, on_delete=models.CASCADE,null=True, related_name='+')
    ruchdst =  models.ForeignKey(Ruches, on_delete=models.CASCADE,null=True, related_name='+')
    nat_cadre = models.ManyToManyField(Nat_cadres)
    nb_cadres = models.IntegerField()
    Commentaire =  models.CharField(max_length=70, blank=True, null=False)

    def __str__(self):
        return self.idmvt


#    def id_mvtcad(self):
#        mvt_cadre = Mvts_cadres.objects.annotate(
#               idmvt= Concat(
#                   value|date_ech:"date
#        return self.objects.filter(f0in__isnull=True)
   
#   def get_absolute_url(self):
#        return reverse('gestruc:Affruruchcol_detail', kwargs={'pk': str(self.pk)})
#
#    def __str__(self):
#       return 'ruche={0}, colonie={1}'.format(self.ruche.code_ruche, self.colonie.code_col)
#

