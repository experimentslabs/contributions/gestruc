
from django import forms
from django.forms.utils import ErrorList
from .models import Site, Departement, Commune, Ruchers, Ruches, Colonies, Visites, Ruruchcol, Interv, Nourrissement


class ParagraphErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()
    def as_divs(self):
        if not self: return ''
        return '<div class="errorlist">%s</div>' % ''.join(['<p class="small error">%s</p>' % e for e in self])



class SiteForm(forms.ModelForm):
    class Meta:
        model = Site
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['commune'].queryset = Commune.objects.none()

        if 'departement' in self.data:
            try:
                departement_id = int(self.data.get('departement'))
                self.fields['commune'].queryset = Commune.objects.filter(insee_dep=departement_id).order_by('nom_dep_m')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['commune'].queryset = self.instance.departement.commune_set.order_by('nom_com_m')



class RucherForm(forms.ModelForm):
    class Meta:
        model = Ruchers
        exclude = ('geom',)


class RucheForm(forms.ModelForm):
    class Meta:
        model = Ruches
        fields = '__all__'


class ColonieForm(forms.ModelForm):
    class Meta:
        model = Colonies
        fields = '__all__'


# class VisiteForm(forms.ModelForm):
#     class Meta:
#         model = Visites
#         fields = '__all__'

class VisiteDetailForm(forms.ModelForm):
    class Meta:
        model = Visites
 #      fields = ('datevis', 'ide_visit', 'operateur', 'rucher','affectation')
        fields = '__all__' 


class VisiteForm(forms.ModelForm):
    class Meta:
        model = Visites
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['affectation'].queryset = Ruruchcol.objects.none()

        if 'rucher' in self.data:
            try:
                rucher_id = int(self.data.get('rucher'))
                self.fields['affectation'].queryset = Ruruchcol.objects.filter(rucher=rucher_id)#.order_by('rucher')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['affectation'].queryset = self.instance.rucher.affectation_set.order_by('ruche')


class AffruruchcolForm(forms.ModelForm):
    class Meta:
        model = Ruruchcol
        fields = '__all__'

    def clean_fin(self):
        ddebut = self.cleaned_data['debut']
        dfin = self.cleaned_data['fin']
        if dfin != None and ddebut > dfin:
            raise forms.ValidationError("la date de fin doit être postérieure à la date de début")
            pass

    def clean_ruche(self):
        druch = self.cleaned_data['ruche']
        rruch = Ruruchcol.objects.filter(fin__isnull = True).filter(ruche_id = druch)
        if rruch.count() != 0 :
            raise forms.ValidationError("la ruche est déjà affectée ")
        return druch

    def clean_colonie(self):
        dcol = self.cleaned_data['colonie']
        rcol = Ruruchcol.objects.filter(fin__isnull = True).filter(colonie_id = dcol)
        if rcol.count() != 0 :
            raise forms.ValidationError("la colonie est déjà affectée ")
            return rcol
        return dcol

class IntervForm(forms.ModelForm):

    class Meta:
        model = Interv
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['affectation'].queryset = Ruruchcol.objects.none()

        if 'rucher' in self.data:
            try:
                rucher_id = int(self.data.get('rucher'))
                self.fields['affectation'].queryset = Ruruchcol.objects.filter(rucher=rucher_id).order_by('rucher')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['affectation'].queryset = self.instance.rucher.affectation_set.order_by('ruche')


class NourForm(forms.ModelForm):

    class Meta:
        model = Nourrissement
        fields = '__all__'
       #affectation = forms.MultipleChoiceField(choices=Nourrissement.affectation, widget=forms.CheckboxSelectMultiple())

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['affectation'].queryset = Ruruchcol.objects.none()

            if 'rucher' in self.data:
                try:
                    rucher_id = int(self.data.get('rucher'))
                    self.fields['affectation'].queryset = Ruruchcol.objects.filter(rucher=rucher_id).order_by('rucher')
                except (ValueError, TypeError):
                    pass  # invalid input from the client; ignore and fallback to empty City queryset
            elif self.instance.pk:
                self.fields['affectation'].queryset = self.instance.rucher.affectation_set.order_by('ruche')

