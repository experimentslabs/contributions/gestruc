from django.contrib.gis import admin
#from geoportal import admin

# # Register your models here.
from .models import Communes, Ruchers, Ruches, Typ_ruche, Phenotypes
from .models import Note_agress, Note_essaim, Note_prod, Note_varroa
from .models import Typ_visite, Etat_ponte, Etat_couv_ouvert, Colonies
from .models import Etat_couv_ferme, Etat_miel, Etat_pollen,Activite_reine, Motif_visite
from .models import Operateur, Note_globale, Visites, Pression_frelon, Gestion_hausses
from .models import Unites, Typ_nourrissement, Nat_nourrissement, Nat_cadres



#admin.site.register(Typ_ruche)
# admin.site.register(Ruchers)


admin.site.register(Communes)

class SampleAdmin(admin.OSMGeoAdmin):
#class SampleAdmin(admin.GeoPortalAdmin):

    fieldsets = [
    ('Identification', {'fields': ['nom_rucher', 'code_rucher']}),
    ('Cartographie', {'fields': ['lieudit', 'commune', 'statut_rucher']}),
    ('OpenStreetMap', {'fields': ['geom']}),
    ]
admin.site.register(Ruchers, admin.OSMGeoAdmin)

admin.site.register(Ruches)
admin.site.register(Gestion_hausses)
admin.site.register(Typ_ruche)
admin.site.register(Phenotypes)
admin.site.register(Note_agress)
admin.site.register(Note_essaim)
admin.site.register(Note_prod)
admin.site.register(Note_varroa)
admin.site.register(Colonies)
admin.site.register(Typ_visite)
admin.site.register(Etat_ponte)
admin.site.register(Etat_couv_ouvert)
admin.site.register(Etat_couv_ferme)
admin.site.register(Etat_miel)
admin.site.register(Etat_pollen)
admin.site.register(Activite_reine)
admin.site.register(Operateur)
admin.site.register(Note_globale)
admin.site.register(Visites)
admin.site.register(Pression_frelon)
admin.site.register(Motif_visite)
admin.site.register(Unites)
admin.site.register(Typ_nourrissement)
admin.site.register(Nat_nourrissement)
admin.site.register(Nat_cadres)
