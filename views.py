from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.template import loader
from django.db import transaction, IntegrityError
from django.shortcuts import get_object_or_404, render, redirect
from .models import Commune, Departement, Site, Ruchers, Ruches, Colonies, Visites, Ruruchcol, Interv, Nourrissement
from gestruc.forms import SiteForm, RucherForm, RucheForm, ColonieForm, VisiteDetailForm, VisiteForm, AffruruchcolForm, ParagraphErrorList, IntervForm, NourForm
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic import FormView, ListView, CreateView, DeleteView, UpdateView, TemplateView


################ vues des sites


class SiteListView(ListView):
    model = Site
    context_object_name = 'site'

class SiteCreateView(CreateView):
    model = Site
    form_class = SiteForm
    # fields = ('nom_site', 'code_site', 'departement', 'commune')
    success_url = reverse_lazy('gestruc:sites_list')

class SiteUpdateView(UpdateView):
    model = Site
    form_class = SiteForm
    # fields = ('nom_site', 'code_site', 'departement', 'commune')
    success_url = reverse_lazy('gestruc:sites_list')

def load_communes(request):
    departement_id = request.GET.get('departement')
    communes = Commune.objects.filter(insee_dep=departement_id).order_by('nom_com_m')
    return render(request, 'gestruc/commune_dropdown_list_options.html', {'communes': communes})

#################

def miellerie(request):
    message= "miellerie"
    context = {'message': message}
    return render(request, 'gestruc/miellerie.html', {'message' : message})

def interventions(request):
    message= "interventions"
    context = {'message': message}
    return render(request, 'gestruc/interventions.html', {'message' : message})


#def index(request):
#    message = "vous êtes sur la page INDEX"
#    context = { 'message': message }
#    template = loader.get_template('gestruc/index.html')
#    return HttpResponse(template.render(context, request=request))
#
class IndexView(TemplateView):
    template_name = "gestruc/index.html"

def toto(request):
    message = "vous êtes sur la page TOTO"
    context = { 'message': message }
    template = loader.get_template('gestruc/toto.html')
    return HttpResponse(template.render(context, request=request))


def nourrissement(request):
    message= "nourrissement"
    context = {'message': message}
    return render(request, 'gestruc/nour_list.html', {'message' : message})


def essaim(request):
    message= "essaim"
    context = {'message': message}
    return render(request, 'gestruc/essaim.html', {'message' : message})


def remerage(request):
    message= "remerage"
    context = {'message': message}
    return render(request, 'gestruc/remerage.html', {'message' : message})


def mvt_cadres(request):
    message= "mvt_cadres"
    context = {'message': message}
    return render(request, 'gestruc/mvt_cadres.html', {'message' : message})


def varroa(request):
    message= "varroa"
    context = {'message': message}
    return render(request, 'gestruc/varroa.html', {'message' : message})


def recolte(request):
    message= "recolte"
    context = {'message': message}
    return render(request, 'gestruc/recolte.html', {'message' : message})

##############   vues des ruchers       ##############


def ruchers_list(request):
    ruchers = Ruchers.objects.all().order_by('pk')
#   for r in ruchers:
#        nbruc = Ruches.objects.filter(affectation=r.id).count()

    context = {
        'ruchers': ruchers,
 #       'nbruc' : nbruc,
    }
    template = loader.get_template('gestruc/ruchers_list.html')
    return HttpResponse(template.render(context, request=request))



def validation(request):
    message= "validation"
    context = {'message': message}
    return render(request, 'gestruc/validation.html', {'message' : message})



######### vues des ruchers
# la partie création doit être migrée dans l'interface ADMIN


def rucher_detail(request, pk, template_name='gestruc/rucher_detail.html'):
    rucher= get_object_or_404(Ruchers, pk=pk)
    form = RucherForm(request.GET or None, instance=rucher)
    ruchcol = Ruruchcol.objects.filter(rucher_id=rucher.pk).filter(fin__isnull=True)
    nbruc = ruchcol.count()

    context = {
        'form':form,
        'ruchcol' : ruchcol,
        'nbruc' : nbruc,
    }
    return render(request, template_name, context)


def rucher_create(request, template_name='gestruc/rucher_add.html'):
    form = RucherForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('gestruc:ruchers')
    return render(request, template_name, {'form':form})


def rucher_update(request, pk, template_name='gestruc/rucher_update.html'):
    rucher= get_object_or_404(Ruchers, pk=pk)
    form = RucherForm(request.POST or None, instance=rucher)
    if form.is_valid():
        form.save()
        return redirect('gestruc:ruchers')
    return render(request, template_name, {'form':form})


def rucher_delete(request, pk, template_name='gestruc/rucher_confirm_delete.html'):
    rucher= get_object_or_404(Ruchers, pk=pk)
    if request.method=='POST':
        rucher.delete()
        return redirect('gestruc:ruchers')
    return render(request, template_name, {'object':rucher})


############ vues des ruches


def ruches_list(request):
    ruches = Ruches.objects.all().order_by('pk')
    context = {'ruches': ruches}
    template = loader.get_template('gestruc/ruches_list.html')
    return HttpResponse(template.render(context, request=request))

def ruche_detail(request, pk, template_name='gestruc/ruche_detail.html'):
    ruche = get_object_or_404(Ruches, pk=pk)
    form = RucheForm(request.GET or None, instance=ruche)
    ruchcol = Ruruchcol.objects.filter(ruche_id=ruche.pk).order_by('-fin')
    context = {
        'form':form,
        'ruche':ruche,
        'ruchcol':ruchcol,
    }
    return render(request, template_name, context)


#################    Vues des colonies ################################


def colonie_detail(request, pk, template_name='gestruc/colonie_detail.html'):
    colonie = get_object_or_404(Colonies, pk=pk)
    form = ColonieForm(request.GET or None, instance=colonie)
    ruchcol = Ruruchcol.objects.filter(colonie_id=colonie.pk).order_by('-fin')

    context = {
        'form':form,
        'colonie':colonie,
        'ruchcol':ruchcol,

    }
    return render(request, template_name, context)


#### vues des visites  ######################### #########################


def visite_detail(request, pk, template_name='gestruc/visite_detail.html'):
    visite = get_object_or_404(Visites, pk=pk)
    form = VisiteDetailForm(request.GET or None, instance=visite)
    context = {
        'form':form,
        'visite':visite,
    }
    return render(request, template_name, context)

# def visite_create(request, template_name='gestruc/visite_add.html'):
#     form = VisiteForm(request.POST or None)
#     context = {
#         'form':form,
#             }
#     if form.is_valid():
#         form.save()
#         return redirect('gestruc:visites' )
#     return render(request, template_name, context)



class VisiteCreateView(CreateView):
    model = Visites
    form_class = VisiteForm
    # fields = ('nom_site', 'code_site', 'departement', 'commune')
    success_url = reverse_lazy('gestruc:interventions')


def visites_list(request):
    visites = Visites.objects.all().order_by('-datevis')
    context = {
                'visites': visites,
         }
    template = loader.get_template('gestruc/visites_list.html')
    return HttpResponse(template.render(context, request=request))


#############  Vues des affectations : ruruchcol


def aff_ruruchcol_detail(request, pk, template_name='gestruc/aff_ruruchcol_detail.html'):
    ruruchcol = get_object_or_404(Ruruchcol, pk=pk)
    form = AffruruchcolForm(request.POST or None, instance=ruruchcol, error_class=ParagraphErrorList)
    context = {
        'form':form,
        'ruruchcol': ruruchcol,
    }
    return render(request, template_name, context)


def aff_ruruchcol_create(request, template_name='gestruc/aff_ruruchcol_add.html'):
    # form = AffruruchcolForm(request.POST or None, error_class=ParagraphErrorList)
     form = AffruruchcolForm(request.POST or None)
     if form.is_valid():
         form.save()
         return redirect('gestruc:aff_list')
     return render(request, template_name, {'form':form})
 
 
def aff_ruruchcol_delete(request, pk, template_name='gestruc/aff_ruruchcol_confirm_delete.html'):
    aff = get_object_or_404(Ruruchcol, pk=pk)
    if request.method == 'POST':
        aff.delete()
        return redirect('gestruc:aff_list')
    return render(request, template_name, {'object':aff})


def aff_ruruchcol_fin(request, pk, template_name='gestruc/aff_ruruchcol_confirm_fin.html'):
    aff = get_object_or_404(Ruruchcol, pk=pk)
    if request.method == 'POST':
        aff.fin = timezone.now()
        aff.save()
        return redirect('gestruc:aff_hist')
    return render(request, template_name, {'object':aff})


def aff_list(request):
    afflist = Ruruchcol.objects.filter(fin__isnull=True).order_by('rucher_id')
    context = {
                'afflist': afflist,
            }
    template = loader.get_template('gestruc/aff_list.html')
    return HttpResponse(template.render(context, request=request))

def aff_hist(request):
    affhist = Ruruchcol.objects.all().order_by('-debut')
    context = {
                'affhist': affhist,
            }
    template = loader.get_template('gestruc/aff_hist.html')
    return HttpResponse(template.render(context, request=request))


def rech_aff(request, template_name='gestruc/aff_ruruchcol_add.html'):
    form = AffruruchcolForm(request.POST or None)
    if form.is_valid():
        druch = self.cleaned_data['ruche']
        afflist = Ruruchcol.objects.filter(fin__isnull = True).filter(ruche_id = druch)

        return redirect('gestruc:aff_list')
    return render(request, template_name, {'form':form})


##########       vues des interventions


class IntervCreateView(CreateView):
    model = Interv
    form_class = IntervForm
    # fields = ('nom_site', 'code_site', 'departement', 'commune')
    success_url = reverse_lazy('gestruc:interventions')


def load_affectation(request):
    rucher_id = request.GET.get('rucher')
    affectation = Ruruchcol.objects.filter(fin__isnull = True).filter(rucher_id=rucher_id)
    return render(request, 'gestruc/affectation_dropdown_list_options.html', {'affectation': affectation})

#
#def nour_detail(request, pk, template_name='gestruc/nour_detail.html'):
#    nour = get_object_or_404(Nourrissement, pk=pk)
#    form = NourForm(request.GET or None, instance=nour)
#    context = {
#        'form':form,
#        'nour':nour,
#    }
#    return render(request, template_name, context)


#class NourCreateView(CreateView):
#    model = Nourrissement
#    form_class = NourForm
#    # fields = ('nom_site', 'code_site', 'departement', 'commune')
#    success_url = reverse_lazy('gestruc:nour_list')
#
#def nour_list(request):
#    nourr = Nourrissement.objects.all()
#    context = {
#                'nourr' : nourr,
#         }
#    template = loader.get_template('gestruc/nour_list.html')
#    return HttpResponse(template.render(context, request=request))
##
#class NourListView(ListView):
#    model = Nourrissement
#    template_name = "gestruc/nour_list.html"
#    nourr = Nourrissement.objects.all()
#    context = {
#                 'nourr' : nourr,
#        }
# 

class NourrissementList(ListView):
    model = Nourrissement


class NourrissementCreate(CreateView):
    model = Nourrissement
    fields = '__all__'
    success_url = reverse_lazy('gestruc:nour_list')


class NourrissementUpdate(UpdateView):
    model = Nourrissement
    fields = '__all__'
    success_url = reverse_lazy('gestruc:nour_list')


class NourrissementDelete(DeleteView):
    model = Nourrissement
    success_url = reverse_lazy('gestruc:nour_list')
