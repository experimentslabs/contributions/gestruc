from django.apps import AppConfig


class GestrucConfig(AppConfig):
    name = 'gestruc'

class GeoportalConfig(AppConfig):
    name = 'geoportal'

