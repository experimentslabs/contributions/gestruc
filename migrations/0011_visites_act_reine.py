# Generated by Django 3.0.2 on 2020-02-02 11:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0010_auto_20200202_1239'),
    ]

    operations = [
        migrations.AddField(
            model_name='visites',
            name='act_reine',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Activite_reine'),
        ),
    ]
