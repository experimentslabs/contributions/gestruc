# Generated by Django 3.0.2 on 2020-03-08 14:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0016_auto_20200204_2209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='communes',
            name='nom_com',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='ruchers',
            name='lieudit',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
