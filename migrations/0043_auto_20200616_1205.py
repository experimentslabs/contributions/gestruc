# Generated by Django 3.0.2 on 2020-06-16 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0042_auto_20200429_2006'),
    ]

    operations = [
        migrations.AddField(
            model_name='colonies',
            name='affectation',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='ruches',
            name='affectation',
            field=models.BooleanField(default=False),
        ),
    ]
