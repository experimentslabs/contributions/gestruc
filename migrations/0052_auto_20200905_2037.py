# Generated by Django 3.0.2 on 2020-09-05 18:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0051_motif_visite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='visites',
            name='libelle',
        ),
        migrations.AddField(
            model_name='visites',
            name='motif_visite',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Motif_visite'),
        ),
    ]
