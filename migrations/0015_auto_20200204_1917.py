# Generated by Django 3.0.2 on 2020-02-04 18:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0014_remove_colonies_rucher'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ruches',
            name='affectation',
        ),
        migrations.RemoveField(
            model_name='ruches',
            name='emplacement',
        ),
    ]
