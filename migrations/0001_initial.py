# Generated by Django 3.0.2 on 2020-02-01 19:25

import datetime
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Communes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('insee_com', models.CharField(blank=True, max_length=5, null=True)),
                ('insee_dep', models.CharField(blank=True, max_length=3, null=True)),
                ('nom_com_m', models.CharField(blank=True, max_length=50, null=True)),
                ('nom_com', models.CharField(blank=True, max_length=45, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note_agress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note_essaim',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note_etat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note_prod',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Note_varroa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Operateur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pseudo', models.CharField(max_length=20)),
                ('nom', models.CharField(blank=True, max_length=20, null=True)),
                ('prenom', models.CharField(blank=True, max_length=20, null=True)),
                ('infos', models.TextField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Phenotypes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phenotype', models.CharField(blank=True, max_length=10, null=True)),
                ('description', models.TextField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Typ_ruche',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modele', models.CharField(blank=True, max_length=30, null=True)),
                ('description', models.TextField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ruches',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code_ruche', models.CharField(blank=True, max_length=10, null=True)),
                ('date_mes', models.DateField(default=datetime.date.today, verbose_name='Date mise en service')),
                ('statut_bio', models.BooleanField(blank=True, default=False)),
                ('commentaire', models.TextField(blank=True, max_length=200, null=True)),
                ('type_ruche', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Typ_ruche')),
            ],
        ),
        migrations.CreateModel(
            name='Ruchers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom_rucher', models.CharField(blank=True, max_length=25, null=True)),
                ('code_rucher', models.CharField(blank=True, max_length=20, null=True)),
                ('dept', models.CharField(blank=True, max_length=2, null=True)),
                ('lieudit', models.CharField(blank=True, max_length=20, null=True)),
                ('statut_rucher', models.BooleanField(blank=True, default=False)),
                ('bio', models.BooleanField(blank=True, default=False)),
                ('geom', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=4326)),
                ('commune', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Communes')),
            ],
        ),
        migrations.CreateModel(
            name='Colonies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code_col', models.CharField(blank=True, max_length=10)),
                ('rucher', models.CharField(blank=True, max_length=10, null=True)),
                ('commentaire', models.TextField(blank=True, max_length=200, null=True)),
                ('agressivite', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Note_agress')),
                ('essaimage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Note_essaim')),
                ('phenotype', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Phenotypes')),
                ('production', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Note_prod')),
                ('varroa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Note_varroa')),
            ],
        ),
    ]
