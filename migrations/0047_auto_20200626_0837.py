# Generated by Django 3.0.2 on 2020-06-26 06:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0046_interv_rucher'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gestion_hausses',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(max_length=20, null=True)),
                ('description', models.TextField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='visites',
            name='colonie',
        ),
        migrations.RemoveField(
            model_name='visites',
            name='ruche',
        ),
        migrations.AddField(
            model_name='visites',
            name='affectation',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Ruruchcol'),
        ),
        migrations.AddField(
            model_name='visites',
            name='libelle',
            field=models.TextField(blank=True, max_length=25),
        ),
        migrations.AddField(
            model_name='visites',
            name='Hausses',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='gestruc.Gestion_hausses'),
        ),
    ]
