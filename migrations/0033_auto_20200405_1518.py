# Generated by Django 3.0.2 on 2020-04-05 13:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0032_auto_20200329_1555'),
    ]

    operations = [
        migrations.AddField(
            model_name='colonies',
            name='existante',
            field=models.BooleanField(default=True),
        ),
        migrations.CreateModel(
            name='Ruruche',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('emplacement', models.CharField(blank=True, max_length=10, null=True)),
                ('debut', models.DateTimeField(verbose_name='Date début')),
                ('fin', models.DateTimeField(verbose_name='Date de fin')),
                ('ruche', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Ruches')),
                ('rucher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Ruchers')),
            ],
        ),
        migrations.CreateModel(
            name='Ruchcolo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.DateTimeField(verbose_name='Date début')),
                ('fin', models.DateTimeField(verbose_name='Date de fin')),
                ('colo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Colonies')),
                ('ruch', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gestruc.Ruches')),
            ],
        ),
    ]
